require 'spec_helper'
require 'tee_stream'

RSpec.describe TeeStream do
  let(:script) do <<~TEST_SCRIPT
    3.times do
      puts "out"
      warn "err"
    end

    exit 2
  TEST_SCRIPT
  end

  describe '.exec' do
    let(:cmd) { %W[ruby -e #{script}] }

    it 'captures command status, output and error streams' do
      null = File.open(IO::NULL, 'w')

      result = TeeStream.exec(cmd, out: null, err: null)

      expect(result.status.success?).to be_falsey
      expect(result.status.exitstatus).to eq(2)
      expect(result.stdout.string).to eq("out\nout\nout\n")
      expect(result.stderr.string).to eq("err\nerr\nerr\n")
    end

    it 'writes command stdout and stderr to provided streams' do
      mock_out = StringIO.new
      mock_err = StringIO.new

      TeeStream.exec(cmd, out: mock_out, err: mock_err)

      expect(mock_out.string).to eq("out\nout\nout\n")
      expect(mock_err.string).to eq("err\nerr\nerr\n")
    end

    context 'when command is not an array' do
      it 'raises error' do
        expect { TeeStream.exec("ruby -e 'puts 1'") }.to raise_error(ArgumentError, 'System command must be an array of strings')
      end
    end
  end
end
