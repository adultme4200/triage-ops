# GitLab triage operations

*This is a prototype. The goal is to add immediate value to the GitLab Engineering function, while determining if it adds value to customers. If so, we will work with Product Management to [bring this functionality to GitLab the product](https://gitlab.com/groups/gitlab-org/-/epics/636).*

Triage operations for GitLab issues and merge requests. This is powered by <https://gitlab.com/gitlab-org/gitlab-triage>.

## The bot

We're using [@gitlab-bot](https://gitlab.com/gitlab-bot) as the user to run
triage operations. The credentials could be found in the shared 1Password
vault.

The same bot is also used in the following projects:

* https://gitlab.com/gitlab-org/release-tools
* https://gitlab.com/gitlab-org/async-retrospectives
* https://gitlab.com/gitlab-com/gl-infra/triage-ops

## The schedules

Schedules are managed automatically by the `schedules-sync` job which runs for
every non-scheduled `master` commit. A `dry-run:schedules-sync` job also runs on
any non-`master` pipeline.

These jobs run the `bin/manage_schedules` script, which allows to sync pipeline
schedules and their variables.

Additionally, it will automatically disable all the pipeline schedules that
aren't declared.

Schedules are defined in the `pipeline-schedules.yml` file. Its format is as follows:

```yaml
---
gitlab-org:
  base:
    cron: '0 4 * * 1-5'
    variables:
      TRIAGE_SOURCE_TYPE: groups
      TRIAGE_SOURCE_PATH: 9970
  variants:
    - id: 11219
      ref: 'feature'
      cron: '0 4 * * 1-7'
      cron_timezone: 'Europe'
      active: true
      variables:
        TRIAGE_LABEL_COMMUNITY_CONTRIBUTIONS: 1
    - active: false
      description: 'gitlab-org weekly schedule'
      cron: '0 0 * * 1'
      variables:
        TRIAGE_TEAM_SUMMARY: 1
```

1. `base` are shared attributes and variables for all the variants of a schedule.
   - Supported attributes are `ref`, `cron`, `cron_timezone`, `active`.
1. `variants` is an array of variants for which you can define/override attributes
    and variables (e.g. useful to define a daily and a weekly job for the same group/project)
   - Supported attributes are the ones from `base`, and additionally `id` and `description`.
   - Defining an `id` allows to update an existing pipeline schedule `description`,
     otherwise it would be impossible since pipeline schedules are matched based on
     their description if no `id` is provided.
1. If `description` is omitted, it will use a default description of
   `[MANAGED] <schedule key> (<variables list>)`, e.g. for the first variant in
   the above example, that would be `[MANAGED] 'gitlab-org' ('TRIAGE_LABEL_COMMUNITY_CONTRIBUTIONS=1')`.
1. Other attributes also have a default value:
   - `ref='master'`
   - `cron_timezone='UTC'`
   - `cron='0 0 * * 1-5'` (Monday to Friday at midnight)
   - `active=true`
1. To temporarily disable a pipeline schedule, just set `active: false`.

## Technical documentation

This section contains some guidance on how to do some current steps to test and setup new triage pipelines.

### Testing with a dry-run

When creating or changing a policy, you are able to test it by leveraging the pipelines within your merge request.

#### Preconditions

1. If you are changing or creating a new policy, be sure to open your merge request first as WIP.
1. Ensure there is data available for your test condition.

#### Steps

1. Open the `dry-run:custom` job in your merge request pipeline.
1. Enter at least the `TRIAGE_POLICY_FILE` variable with the path of the policy file you want to test, for instance:
   - Fill the `Key` field with `TRIAGE_POLICY_FILE`.
   - Fill the `Value` field with `policies/stages/hygiene/label-missed-slo.yml`.
1. By default the dry-run will run against the `gitlab-org/gitlab` project. If you want to change that, you can add:
   - A `TRIAGE_SOURCE_TYPE` variable with the value `projects` (default) or `groups`.
   - A `TRIAGE_SOURCE_PATH` variable with the path or ID of the project or group that you want to test the new policy against.
1. Click "Trigger this manual action" and enjoy! 🍿

### Setting up a new pipeline schedule

Creating a scheduled pipeline is done programmatically, via a merge request and can be done at the same time a new policy is introduced (or not).

Refer to the `The schedules` section above for more details.

#### Testing the schedule with a dry-run pipeline

Similar to testing with a dry-run for a policy file, we can also trigger a
pipeline using the same variables configured in the schedule.

1. Open the `dry-run:schedule` job in your merge request pipeline.
1. Enter `TRIAGE_SCHEDULE_NAME` variable with the path of schedule you want to test, for instance:
   - Fill the `Key` field with `TRIAGE_SCHEDULE_NAME`.
   - Fill the `Value` field with `gitlab-org/build/cng`
1. (Optional) If there are multiple schedules with the same name, we can also
   pass `TRIAGE_SCHEDULE_INDEX` to indicate which one we want to run.
   By default, it's the first one, which the index is `0`.
1. (Optional) If we want to add some additional variables to the triggered
   pipeline, we can prefix those variables with `BYPASSING_` so they will be
   passed along to the triggered pipeline without the prefix. For example,
   suppose we want to add `TRIAGE_FAKE_TODAY_FOR_MISSED_RESOURCES=2020-01-01`
   to the triggered pipeline, we can:
   - Fill the `Key` field with `BYPASSING_TRIAGE_FAKE_TODAY_FOR_MISSED_RESOURCES`.
   - Fill the `Value` field with `2020-01-01`
1. This will use the same variables defined as in the schedule, along with `DRY_RUN=1` to make sure everything should be running in dry-run mode.
1. Click "Trigger this manual action" and you can see the triggered pipeline
  URL printed in the job log. You can also go to the same pipeline page and
  check the triggered downstream pipeline.

### Generating policy files and CI jobs

Due to some [technical limitation](https://gitlab.com/gitlab-org/gitlab-triage/-/issues/191),
it's much easier to generate policy files and CI jobs via scripts.

We once [tried to generate a child pipeline](https://gitlab.com/gitlab-org/quality/triage-ops/-/merge_requests/483#note_337672436)
to run the generated CI jobs, but it turned out a child pipeline cannot
generate another child pipeline, yet. Thus it's much easier if we just
commit them into the repository.

Here's the script which can generate everything in this repository:

```shell
bin/generate_group_policies -t policies/template/idle-merge-requests.yml.erb
bin/generate_group_ci_jobs -t .gitlab/ci/template/idle-merge-requests.yml.erb
```

We may add a job to verify that all changes to the template should be
reflected to the generated and committed files in the future.
