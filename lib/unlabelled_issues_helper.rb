# frozen_string_literal: true

module UnlabelledIssuesHelper
  POTENTIAL_TRIAGERS =
    %w(
      @a_mcdonald
      @at.ramya
      @caalberts
      @chloeliu
      @dchevalier2
      @ddavison
      @ebanks
      @godfat
      @grantyoung
      @jennielouie
      @jo_shih
      @kwiebers
      @markglenfletcher
      @mlapierre
      @niskhakova
      @rymai
      @sliaquat
      @svistas
      @tmslvnkc
      @tpazitny
      @treagitlab
      @vincywilson
      @willmeek
      @zeffmorgan
    ).freeze

  def distribute_items(list_items)
    POTENTIAL_TRIAGERS.shuffle
      .zip(list_items.each_slice((list_items.size.to_f / POTENTIAL_TRIAGERS.size).ceil))
      .sort { |(triager_a, _), (triager_b, _)| triager_a <=> triager_b }
      .to_h
      .compact
  end
end
