# frozen_string_literal: true

require 'erb'
require 'fileutils'

require_relative '../group_definition'

module Generate
  module GroupCiJob
    def self.run(options)
      template_name = File.basename(options.template)
      erb = ERB.new(File.read(options.template), nil, '-')
      policy_base = "policies/generated/#{template_name}"

      FileUtils.mkdir_p(destination)

      groups = GroupDefinition::DATA.each_key.
        each_with_object({}) do |name, result|
          result[name] = "#{policy_base}/#{name}.yml"
        end

      File.write(
        "#{destination}/#{template_name}.yml",
        erb.result_with_hash(groups: groups)
      )
    end

    def self.destination
      @destination ||=
        File.expand_path('generated', "#{__dir__}/../../.gitlab/ci")
    end
  end
end
